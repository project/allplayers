<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */


/**
 * Act on AllPlayers account actions
 *
 * This hook allows you modules to react when operations regarding the
 * connection of the AllPlayers account to the local account are performed.
 *
 * @param $op
 *   What kind of action is being performed. Possible values (in alphabetical
 *   order):
 *   - after_connect: A connection has been formed in the in authmap table
 *     between the local uid and the AllPlayers uuid.
 *   - after_login: A successful login has been performed using AllPlayers.
 *
 * @param $apuser
 *   The AllPlayers user object returned from the API.
 *
 * @param $matched_user
 *   The matched local user object.
 */

function hook_allplayers_user_connect($op, $apuser, $matched_user) {
  switch($op) {
    case 'after_connect':

      break;

    case 'after_login':

      break;
  }
}

/**
 * @}
 */
