## AllPlayers.com OAuth

Connect to [AllPlayers.com API via OAuth](http://develop.allplayers.com/oauth.html) authorization. This is ideal for "seamless" API authorization from user accounts and access.

### Development

Thanks for the inspiration initial direction from [fboauth Drupal module](http://drupal.org/project/fboauth).

Popup code for OAuth window "overlay": https://github.com/zuzara/jQuery-OAuth-Popup

Note this project uses [composer](http://getcomposer.org/) to manage dependencies.

Quick start:

```
git clone --recursive --branch 6.x-1.x http://git.drupal.org/project/AllPlayers.git
```

The composer dependencies are already included with the module repo. However, if you choose to update them or wish to change them,
```
composer.phar install
```
*For more information about composer, including installation instructions, visit [the project site](http://getcomposer.org)


##Set Up
*  Verify that the email for your site's user 1 exists on AllPlayers.com, log in as that user, and find their uid.
*  Enable the module on your dev site.
*  Go to your dev site's `/admin/settings/allplayers` and copy the callback URL.
*  Go to `https://www.allplayers.com/users/{your_sites_user_1_uid}/oauth/consumer/add` and set up your oauth token and secret.
*  For the callback URL, paste what you copied from `/admin/settings/allplayers`
*  Copy your token and secret back to the dev site.
*  Hit Save.
*  Place the AllPlayers Login block provided by the module somewhere on the site.



