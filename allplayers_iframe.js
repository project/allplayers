Drupal.behaviors.allplayers_iframe = function(context) {
	function onMessage(messageEvent) {
		var height = messageEvent.data.height + 'px';
		  // Once height message is received from QS, slide open the iframe
		  $('#' + messageEvent.source.targetWindowName).animate({
		    'height' : height
		  }, 200);
		}
		var windowProxies = {};
		window.onload=function(){
			 for (var i in Drupal.settings.allplayers.windowproxy_ids){
			    	var targetId = Drupal.settings.allplayers.windowproxy_ids[i];
			    	// Create a proxy window to send to and receive
				    // messages from the iFrame.
			    	windowProxies[targetId] =  new Porthole.WindowProxy('', targetId);
			    	 // Register an event handler to receive message.
			    	windowProxies[targetId].addEventListener(onMessage);
			 }
	   };
};