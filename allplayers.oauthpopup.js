Drupal.behaviors.allplayersoauthpopup = function(context) {
  $('.allplayers_action_connect_oauthpopup').click(function(event) {
    event.preventDefault();
    var path = $(this).attr('href');
    var windowName = 'ConnectWithOAuth'; // should not include space for IE
    var windowOptions = 'location=0,status=0,width=570,height=700,scrollbars=yes';
    var oauthWindow   = window.open(path, windowName, windowOptions);
    var oauthInterval = window.setInterval(function(){
      if (oauthWindow.closed) {
        window.clearInterval(oauthInterval);
        window.location = Drupal.settings.allplayers.allplayers_redirect;
      }
    }, 1000);
  });
};

