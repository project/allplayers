<?php

/**
 * @file
 * Administrative pages and functions for AllPlayers module.
 */

/**
 * Menu callback; Display the settings form for AllPlayers OAuth.
 */
function allplayers_settings_form(&$form_state) {
  $form['oauth']['callback_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Callback URL'),
    '#description' => t('Provide this URL as the callback in your OAuth Consumer Configuration on AllPlayers'),
    '#value' => url('allplayers/auth', array('absolute' => TRUE)),
    '#disabled' => TRUE,
  );
  $form['allplayers_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer Key'),
    '#size' => 40,
    '#maxlengh' => 50,
    '#description' => t('To enable OAuth based access for AllPlayers.com, you must <a href="@url">register your application</a> with AllPlayers.com and add the provided keys here.', array('@url' => 'http://develop.allplayers.com/oauth.html')) . ' ' . t('Enter your Consumer Key here.'),
    '#default_value' => variable_get('allplayers_key', ''),
  );
  $form['allplayers_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer Secret'),
    '#size' => 40,
    '#maxlengh' => 50,
    '#description' => t('Enter your Consumer Secret here.'),
    '#default_value' => variable_get('allplayers_secret', ''),
  );
  // @todo for group registration, display the # blocks to make available here, and build them in block hooks.
  // Build image selection options
  $img_path = drupal_get_path('module', 'allplayers') . '/assets';
  $results = file_scan_directory($img_path, '.png');
  $options = array(
    'register' => array('link' => '<a href="#">Register</a>'),
    'login' => array('link' => '<a href="#">Login</a>'),
  );
  foreach ($results as $image) {
    $options['login'][$image->basename] = theme('image', $image->filename);
    $options['register'][$image->basename] = theme('image', $image->filename);
  }
  $form['allplayers_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Select login display'),
    '#options' => $options['login'],
    '#default_value' => variable_get('allplayers_button', 'link'),
  );
  $form['allplayers_register_button'] = array(
    '#type' =>  'radios',
    '#title' => t('Select register display'),
    '#options' => $options['register'],
    '#default_value' => variable_get('allplayers_register_button', 'link'),
  );

  return system_settings_form($form);
}

/**
 * Menu callback; Display the settings form for AllPlayers OAuth Actions.
 */
function allplayers_actions_settings_form(&$form_state) {
  global $user;

  $form['allplayers_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorized redirect'),
    '#size' => 20,
    '#description' => t('Local page to redirect user, after authentication.'),
    '#default_value' => variable_get('allplayers_redirect', '<front>'),
  );

  // If the admin user is connected we store this users creds (stored
  //  automatically) to perform actions for a specific group.
  $uuid = allplayers_uuid_load($user->uid);
  $admin_auth = variable_get('allplayers_admin_auth', array());
  if (empty($uuid) || empty($admin_auth)) {
    $form['allplayers_groupjoin'] = array(
      '#type' => 'item',
      '#title' => t('Join Group UUID'),
      '#children' => theme('allplayers_user_form_connect', $uid, ''),
      '#description' => t('Auto join functionality requires your admin user to be connected to AllPlayers.com.'),
    );
  }
  else {
    // Load admin users groups to select from.
    $groups = _allplayers_get_user_groups($admin_auth);
    $options = array('AllPlayers.com Group' => array());
    foreach ($groups as $group) {
      $options['AllPlayers.com Group'][$group->uuid] = $group->title;
    }

    $form['allplayers_groupjoin'] = array(
      '#type' => 'select',
      '#multiple' => FALSE,
      '#options' => $options,
      '#title' => t('Join Group'),
      '#description' => t('Group to auto join a user, once they have registered.'),
      '#default_value' => variable_get('allplayers_groupjoin', ''),
    );
  }

  // If Profile module exists, allow mapping of individual properties.
  if (module_exists('profile')) {
    module_load_include('inc', 'allplayers', '/includes/allplayers.profile');
    allplayers_profile_form_alter($form, $form_state);
  }

  return system_settings_form($form);
}

/**
 * User settings page for AllPlayers OAuth connection.
 */
function allplayers_user_form($account) {
  return theme('allplayers_user_info', $account);
}

/**
 * Provided themed information about the user's current AllPlayers.com connection.
 */
function theme_allplayers_user_info($account) {
  $uuid = allplayers_uuid_load($account->uid);
  $output = '';
  if ($uuid !== FALSE) {
    $output .= '<p>' . t('Your account is current connected with AllPlayers.com.') . '</p>';
  }
  else {
    $output .= '<p>' . t('Your account is not currently connected with AllPlayers.com. To connect this account to AllPlayers.com, click on the link below.') . '</p>';
    $output .= theme('allplayers_action_connect');
  }

  return $output;
}
