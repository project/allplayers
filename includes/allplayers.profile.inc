<?php

/**
 * @file
 * Functions to assist with handling with Profile module data.
 */

/**
 * Return a list of all profile fields supported by Profile module.
 *
 * Profile module doesn't provide a "pretty" way to do this unfortunately,
 * so altogether this is a pretty ugly looking function.
 */
function allplayers_profile_fields() {
  static $profile_fields;

  if (!isset($profile_fields)) {
    $profile_fields = array();
    $profile_categories = profile_categories();
    foreach ($profile_categories as $profile_category) {
      $result = _profile_get_fields($profile_category['name']);
      while ($field = db_fetch_array($result)) {
        $profile_fields[$field['name']] = $field;
      }
    }
  }

  return $profile_fields;
}

/**
 * Add options for Profile module to the AllPlayers.com OAuth settings form.
 */
function allplayers_profile_form_alter(&$form, &$form_state) {
  $form['allplayers_user_profile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile field mapping'),
    '#description' => t('Each of your Profile fields are listed below. Map each one you would like to import into your site from AllPlayers.com. <strong>Note that only profile fields configured to show on the user registration form may be imported!</strong>'),
    '#tree' => TRUE,
    '#weight' => 5,
  );

  $properties = allplayers_user_properties();
  $property_options = array('' => t('- Do not import -'));
  foreach ($properties as $property => $property_info) {
    $property_options[$property] = '[' . $property . '] ' . $property_info['label'];
  }

  $field_defaults = variable_get('allplayers_user_profile', array());
  $field_select = array(
    '#type' => 'select',
    '#options' => $property_options,
  );

  $profile_fields = allplayers_profile_fields();
  foreach ($profile_fields as $profile_name => $profile_field) {
    $form['allplayers_user_profile'][$profile_name] = $field_select;
    $form['allplayers_user_profile'][$profile_name]['#title'] = t($profile_field['title']);
    $form['allplayers_user_profile'][$profile_name]['#default_value'] = isset($field_defaults[$profile_name]) ? $field_defaults[$profile_name] : '';
  }
}

/**
 * Return a list of AllPlayers.com user properties.
 *
 * This function provides a list of properties that may be attached directly to
 * a AllPlayers user account. This information is immediately available when a
 * user logs in with AllPlayers connect and may be stored locally.
 */
function allplayers_user_properties() {
  // Common properties. Always defined so that modules may alter if needed.
  $common = array(
    'uuid' => array(
      'label' => t('AllPlayers.com UUID'),
    ),
    'email' => array(
      'label' => t('E-mail'),
    ),
    'username' => array(
      'label' => t('Full name'),
    ),
    'firstname' => array(
      'label' => t('First name'),
    ),
    'lastname' => array(
      'label' => t('Last name'),
    ),
    'middlename' => array(
      'label' => t('Middle name'),
    ),
    'gender' => array(
      'label' => t('Gender'),
    ),
    'link' => array(
      'label' => t('AllPlayers.com profile link'),
    ),
    'nickname' => array(
      'label' => t('Nickname'),
    ),
    'last_modified' => array(
      'label' => t('Last updated time'),
    ),
    'address+street' => array(
      'label' => t('Address: Street'),
    ),
    'address+city' => array(
      'label' => t('Address: City'),
    ),
    'address+state' => array(
      'label' => t('Address: State'),
    ),
    'address+postal_code' => array(
      'label' => t('Address: Postal Code'),
    ),
    'address+country' => array(
      'label' => t('Address: Country'),
    ),
    'phone+phone' => array(
      'label' => t('Phone'),
    ),
    'phone+phone_cell' => array(
      'label' => t('Phone: Cell'),
    ),
  );
  $properties = array() + $common;

  drupal_alter('allplayers_user_properties', $properties);
  ksort($properties);

  return $properties;
}

/**
 * Add profile info to a Drupal user array (before account creation).
 */
function allplayers_profile_create_user(&$edit, $apuser) {
  $profile_map = variable_get('allplayers_user_profile', array());
  $profile_fields = allplayers_profile_fields();

  foreach (array_filter($profile_map) as $profile_field_name => $allplayers_property_name) {
    if (isset($profile_fields[$profile_field_name])) {
      $profile_field = $profile_fields[$profile_field_name];
      $apuser_allplayers_property_name = '';
      if (strpos($allplayers_property_name, '+') === FALSE) {
        $apuser_allplayers_property_name = $apuser->$allplayers_property_name;
      }
      else {
        $allplayers_property_name = explode('+', $allplayers_property_name);
        $apuser_allplayers_property_name = $apuser->$allplayers_property_name[0]->$allplayers_property_name[1];
      }
      switch ($profile_field['type']) {
        case 'selection':
          // We can't import anything other than strings into selects.
          if (!is_string($apuser_allplayers_property_name)) {
            break;
          }

          // Mapping options is tricky business. We loop through all available
          // options and choose the closest one to match the incoming value.
          $options = explode("\n", $profile_field['options']);
          $best_match = 0.0;
          $best_option = NULL;
          $fb_option = is_string($apuser_allplayers_property_name) ? $apuser_allplayers_property_name : '';
          $match_fb = strtolower($apuser_allplayers_property_name);
          foreach ($options as $option) {
            $option = trim($option);
            $match_option = strtolower($option);
            $this_match = 0;
            similar_text($match_option, $match_fb, $this_match);
            if ($this_match > $best_match) {
              $best_match = $this_match;
              $best_option = $option;
            }
          }
          if (isset($best_option)) {
            $edit[$profile_field_name] = $best_option;
          }
          break;
        case 'textfield':
        case 'textarea':
        case 'list':
        case 'url':
        default:
          // We can't import anything other than strings into textfields.
          if (!is_string($apuser_allplayers_property_name)) {
            break;
          }

          $edit[$profile_field_name] = $apuser_allplayers_property_name;
          break;
      }
    }
  }
}
